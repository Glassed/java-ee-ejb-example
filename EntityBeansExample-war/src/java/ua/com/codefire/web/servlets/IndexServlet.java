/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.web.servlets;

import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ua.com.codefire.beans.entity.Book;
import ua.com.codefire.beans.sessions.BooksDaoBeanLocal;

/**
 *
 * @author blaro
 */
@WebServlet("/index")
public class IndexServlet extends HttpServlet {

    @EJB
    private BooksDaoBeanLocal daoBooksBean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Book> list = daoBooksBean.getAll();
        request.setAttribute("books", list);
        request.getRequestDispatcher("WEB-INF/pages/index.jsp").forward(request, response);
    }

}
