package ua.com.codefire.web.servlets;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ua.com.codefire.beans.entity.Book;
import ua.com.codefire.beans.sessions.BooksDaoBeanLocal;

/**
 *
 *
 */
@WebServlet("/change-book")
public class ChangeServlet extends HttpServlet {

    @EJB
    BooksDaoBeanLocal dao;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        String name = req.getParameter("name");
        String author = req.getParameter("author");
        int pages = Integer.parseInt(req.getParameter("pages"));
        Book bookWithUpdates = new Book(author, name, pages);
        if (dao.update(id, bookWithUpdates) == -1) {
            req.setAttribute("id", id);
            req.setAttribute("message", "Something wrong");
            req.getRequestDispatcher("WEB-INF/pages/change.jsp").forward(req, resp);
            return;
        }
        resp.sendRedirect("index");
        return;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int id = Integer.parseInt(req.getParameter("id"));
        Book book = dao.getById(id);
        req.setAttribute("book", book);
        req.getRequestDispatcher("WEB-INF/pages/change.jsp").forward(req, resp);
        return;
    }

}
