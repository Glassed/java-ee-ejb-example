/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.web.servlets;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ua.com.codefire.beans.entity.Book;
import ua.com.codefire.beans.sessions.BooksDaoBeanLocal;

/**
 *
 * @author blaro
 */
@WebServlet("/add-book")
public class AddServlet extends HttpServlet {

    @EJB
    BooksDaoBeanLocal dao;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String author = request.getParameter("author");
        int pages = Integer.parseInt(request.getParameter("pages"));

        Book book = new Book(author, name, pages);

        if (dao.create(book) == -1) {
            request.setAttribute("message", "Can't add");
            request.getRequestDispatcher("WEB-INF/pages/add.jsp").forward(request, response);
            return;
        }

        response.sendRedirect("index");
        //request.getRequestDispatcher("WEB-INF/pages/index.jsp").forward(request, response);
        return;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher("WEB-INF/pages/add.jsp").forward(request, response);
        return;

    }

}
