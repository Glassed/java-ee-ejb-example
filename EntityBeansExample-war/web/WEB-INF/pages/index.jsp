<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : index
    Created on : Oct 18, 2015, 11:40:52 AM
    Author     : blaro
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>Library</h3>
        <table border="1">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Author</th>
                    <th>Pages</th>
                    <td> </td>
                    <th> </th>

                </tr>
            </thead>
            <tbody>
                <c:forEach var="book" items="${books}">
                    <tr>
                        <td><c:out value="${book.id}" default="name" escapeXml="true"/></td>
                        <td><c:out value="${book.name}" default="name" escapeXml="true"/></td>
                        <td><c:out value="${book.author}" default="name" escapeXml="true"/></td>
                        <td><c:out value="${book.pages}" default="name" escapeXml="true"/></td>
                        <td><a href="remove-book?id=<c:out value="${book.id}" default="name" escapeXml="true"/>">Remove</a></td>
                        <td><a href="change-book?id=<c:out value="${book.id}" default="name" escapeXml="true"/>">Change</a></td>
                    </tr>
                </c:forEach>
                <tr>
                    <td colspan="5" ></td>
                    <td><a href="add-book">Add new</a></td>

                </tr>
            </tbody>
        </table>
    </body>
</html>
