<%-- 
    Document   : remove
    Created on : Oct 23, 2015, 12:14:50 PM
    Author     : Lex
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        Are you sure want to remove this book :


        <table border="1">
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Pages</td>
                <td>Author</td>
                
            </tr>
            <tr>
                <td>
                    <c:out value="${book.id}" default="name" escapeXml="true"/>
                </td>
                <td>
                    <c:out value="${book.name}" default="name" escapeXml="true"/>
                </td>
                <td>
                    <c:out value="${book.pages}" default="name" escapeXml="true"/>
                </td>
                <td>
                    <c:out value="${book.author}" default="name" escapeXml="true"/>
                </td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td>
                    <form method="post">
                        <input type="hidden" value="<c:out value="${book.id}" default="name" escapeXml="true"/>"/>
                        <input type="submit" value="delete"/>
                    </form>

                </td>
            </tr>
        </table>

        <hr>
        ${message}

    </body>
</html>
