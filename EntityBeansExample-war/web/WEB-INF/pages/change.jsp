<%-- 
    Document   : change-book
    Created on : Oct 23, 2015, 1:36:43 PM
    Author     : Lex
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <form method="post">
        <table border="1">
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Pages</td>
                <td>Author</td>
            </tr>
            <tr>
                <td>
                    <c:out value="${book.id}" default="id" escapeXml="true"/>                    
                </td>
                <td>
                    <input type="text" value="<c:out value="${book.name}" default="name" escapeXml="true"/>" name="name"/>
                </td>
                <td>
                    <input type="text" value="<c:out value="${book.pages}" default="pages" escapeXml="true"/>"name="pages"/>
                </td>
                <td>
                    <input type="text" value="<c:out value="${book.author}" default="author" escapeXml="true"/>"name="author"/>
                </td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td>
                    <input type="hidden" value="<c:out value="${book.id}" default="name" escapeXml="true"/>" name="id"/>
                    <input type="submit" value="change"/>
                </td>
            </tr>
        </table>
    </form>


    <hr>
    ${message}

</body>
</html>
