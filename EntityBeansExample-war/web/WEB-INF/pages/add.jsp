<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <form method="post">        
            <table border="1">
                <tr>
                    <td>Name</td>
                    <td>Pages</td>
                    <td>Author</td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="name"/>
                    </td>
                    <td>
                        <input type="text" name="pages"/>
                    </td>
                    <td>
                        <input type="text" name="author"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td>
                        <input type="submit" value="send"/>
                    </td>
                </tr>
            </table>
        </form>

        <hr>
        ${message}

    </body>
</html>
