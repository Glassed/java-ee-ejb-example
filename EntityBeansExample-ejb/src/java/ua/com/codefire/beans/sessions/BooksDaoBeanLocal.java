/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.beans.sessions;

import java.util.List;
import javax.ejb.Local;
import ua.com.codefire.beans.entity.Book;

/**
 *
 * @author blaro
 */
@Local
public interface BooksDaoBeanLocal {

    public List<Book> getAll();

    /**
     *
     * @param book - entity
     * @return 1 if everything is ok and -1 if its not;
     */
    public int create(Book book);

    public Book getById(int id);

    public int delete(int id);

    public int update(int id, Book bookChanges);

}
