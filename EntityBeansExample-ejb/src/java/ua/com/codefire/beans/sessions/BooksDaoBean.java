/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.beans.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import ua.com.codefire.beans.entity.Book;

/**
 *
 * @author blaro
 */
@Stateless
public class BooksDaoBean implements BooksDaoBeanLocal {

    private final EntityManagerFactory factory = Persistence.createEntityManagerFactory("EntityBeansExample-ejbPU");

    @Override
    public List<Book> getAll() {
        List<Book> list;
        EntityManager manager = null;
        try {
            manager = factory.createEntityManager();
            list = manager.createNamedQuery("Book.findAll", Book.class).getResultList();
            return list;
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
    }

    @Override
    public Book getById(int id) {
        Book book;
        EntityManager manager = null;
        try {
            manager = factory.createEntityManager();
            book = manager.createNamedQuery("Book.findById", Book.class).setParameter("id", id).getSingleResult();
            return book;
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
    }

    @Override
    public int create(Book book) {
        EntityManager manager = null;
        try {
            manager = factory.createEntityManager();
            manager.getTransaction().begin();
            manager.persist(book);
            manager.getTransaction().commit();
        } catch (Exception e) {
            return -1;
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
        return 1;
    }

    @Override
    public int delete(int id) {
        EntityManager manager = null;

        try {
            manager = factory.createEntityManager();
            Book book = (Book) manager.find(Book.class, id);
            manager.getTransaction().begin();
            manager.remove(book);
            manager.getTransaction().commit();
        } catch (Exception e) {
            return -1;
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
        return 1;
    }

    @Override
    public int update(int id, Book bookChanges) {
        EntityManager manager = null;

        try {
            manager = factory.createEntityManager();
            Book book = (Book) manager.find(Book.class, id);
            manager.getTransaction().begin();
            book.setAuthor(bookChanges.getAuthor());
            book.setPages(bookChanges.getPages());
            book.setName(bookChanges.getName());
            manager.flush();
            manager.getTransaction().commit();

        } catch (Exception e) {
            return -1;
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
        return 1;
    }

}
